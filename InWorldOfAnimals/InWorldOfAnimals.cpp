// InWorldOfAnimals.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

#include <iostream>
#include <map>
#include <string>

class Animal
{
public:
	virtual const char* get_name() const = 0;
	virtual ~Animal() {}
};

class Cat : public Animal
{
public:
	Cat() {}
	~Cat() {}
	const char* get_name() const;
};

const char* Cat::get_name() const
{
	return "Cat";
}

class Dog : public Animal
{
public:
	Dog() {}
	~Dog() {}
	const char* get_name() const;
};

const char* Dog::get_name() const
{
	return "Dog";
}


class AbstractCreator
{
	public:
		virtual Animal* create() = 0;
};

template <class C1> class AnimalCreator : public AbstractCreator {

	public:
		virtual Animal* create() { return new C1(); }
};


class AnimalFabric
{
	typedef std::map<std::string, AbstractCreator*>::iterator Pointer;

	private:
		std::map<std::string, AbstractCreator*> _crtset;
	public:
		template <class C> bool registrate(const std::string &classname)
		{
			// ������ ��������, ��������, �� ������������ � ���� ! ����� return false
			_crtset[classname] = new AnimalCreator<C>();
			return true;
		}

		Animal* createAnimal(const std::string &classname)
		{
			Pointer p = _crtset.find(classname);
			if (p != _crtset.end())
				return p->second->create();
			else
				return 0;
		}
};



int main()
{
	AnimalFabric *pAF = new AnimalFabric;

	std::string catname("Cat");
	std::string dogname("Dog");

	pAF->registrate<Cat>(catname);
	pAF->registrate<Dog>(dogname);

	Animal *cat = pAF->createAnimal(catname);
	Animal *dog = pAF->createAnimal(dogname);

	const char* cat_str = cat->get_name();
	const char* dog_str = dog->get_name();

    return 0;
}

